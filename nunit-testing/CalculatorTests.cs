using NUnit.Framework;
using CSharpCalculator;
using System;

namespace nunit_testing
{
    [Parallelizable(ParallelScope.All)]
    public class Tests
    {
        public Calculator obj;

        [SetUp]
        public void Setup()
        {
            obj = new Calculator();
        }

        [Test]
        [TestCase(3)]
        [TestCase(-20)]
        [TestCase(0)]
        public void SinFunctionTest(object a)
        {
            Assert.AreEqual(Math.Sin(Convert.ToDouble(a)), obj.Sin(a));
        }
        [Test]
        [TestCase(3)]
        [TestCase(-20)]
        [TestCase(0)]
        public void CosFunctionTest(object a)
        {
            Assert.AreEqual(Math.Cos(Convert.ToDouble(a)), obj.Cos(a));
        }

        [Test]
        [TestCase(3, true)]
        [TestCase(-20, false)]
        [TestCase(0, false)]
        public void IsPositiveFunctionTest(object a, bool expectedResult)
        {
            Assert.AreEqual(expectedResult, obj.isPositive(a));
        }

        [Test]
        [TestCase(3, false)]
        [TestCase(-20, true)]
        [TestCase(0, true)]
        public void IsNegativeFunctionTest(object a, bool expectedResult)
        {
            Assert.AreEqual(expectedResult, obj.isNegative(a));
        }
    }
}