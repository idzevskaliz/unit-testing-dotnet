using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using CSharpCalculator;

namespace unit_testing
{
    [TestClass]
    public class Tests
    {
        public Calculator obj;

        [TestInitialize]
        public void SetUp()
        {
            obj = new Calculator();
        }

        [DataTestMethod]
        [DataRow(-1, 4, 3)]
        [DataRow(0, 0, 0)]
        [DataRow(0.2, 0.5, 0.7)]
        public void SumFunctionTest(object a, object b, double expectedResult)
        {
            Assert.AreEqual(expectedResult, obj.Add(a, b));

        }

        [DataTestMethod]
        [DataRow(-1, 4, -5)]
        [DataRow(0, 0, 0)]
        [DataRow(0.2, 0.5, -0.3)]
        public void SubFunctionTest(object a, object b, double expectedResult)
        {
            Assert.AreEqual(expectedResult, obj.Sub(a, b));

        }

        [DataTestMethod]
        [DataRow(-1, 4, -4)]
        [DataRow(0, 0, 0)]
        [DataRow(0.2, 0.5, 0.1)]
        public void MultiplyFunctionTest(double a, double b, double expectedResult)
        {
            Assert.AreEqual(expectedResult, obj.Multiply(a, b));

        }

        [DataTestMethod]
        [DataRow(-1, 4, -0.25)]
        [DataRow(3, 0, 0)]
        [DataRow(0, 4, 0)]
        [DataRow(0.2, 0.5, 0.4)]
        public void DivideFunctionTest(double a, double b, double expectedResult)
        {
            Assert.AreEqual(expectedResult, obj.Divide(a, b));

        }

        [DataTestMethod]
        [DataRow(-1, 1)]
        [DataRow(2, 2)]
        [DataRow(0, 0)]

        public void AbsFunctionTest(object a, double expectedResult)
        {
            Assert.AreEqual(expectedResult, obj.Abs(a));

        }

        [DataTestMethod]
        [DataRow(-1, 4, -1)]
        [DataRow(1, 0, 1)]
        [DataRow(1, 3.5, 1)]

        public void PowFunctionTest(object a, object b, double expectedResult)
        {
            Assert.AreEqual(expectedResult, obj.Pow(a, b));

        }

        [DataTestMethod]
        [DataRow(9, 3)]
        [DataRow(22, 4.69041575982343)]


        public void SqrtFunctionTest(object a, double expectedResult)
        {
            Assert.AreEqual(expectedResult, obj.Sqrt(a));

        }
    }
}
